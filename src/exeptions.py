class NoDataFoundForCode(Exception):
    def __init__(self, code):
        super().__init__(f'No data in RSA data for {code}')
