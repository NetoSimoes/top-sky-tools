from dotenv import load_dotenv


class Config:
    load_dotenv()
    SECRET_KEY = '5791628bb0b13ce0c676dfde280ba245'


class Development(Config):
    DEBUG = True


class Staging(Config):
    DEBUG = True


class Production(Config):
    DEBUG = False


configs = {
    'development': Development,
    'staging': Staging,
    'production': Production,
}


def configure_app(app, config):
    return app.config.from_object(configs.get(config, Config)())
