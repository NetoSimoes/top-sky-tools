from dataclasses import dataclass
from datetime import datetime


@dataclass
class RSANotice:
    start_date: datetime = datetime(1999, 8, 25, 10, 30)
    end_date: datetime = datetime(1999, 8, 26, 6, 0)
