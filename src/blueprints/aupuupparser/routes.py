from flask import render_template, Blueprint

from src.blueprints.aupuupparser.forms import RegistrationForm
from src.exeptions import NoDataFoundForCode
from src.tools.aupuupparser.manager import Manager

bp = Blueprint('aupuupparser', __name__, template_folder='templates',
               static_folder='static')


@bp.route('/parser', methods=['GET'])
def show_form():
    form = RegistrationForm()

    return render_template('parser.html', title='AUP/UUP Parser', form=form)


@bp.route('/parser', methods=['POST'])
def parse_form():
    form = RegistrationForm()
    if form.validate_on_submit():
        try:
            results = [area for area in Manager(form.rawdata.data, form.countryicaocode.data).area_iterator(
                short_name=form.shortareaname.data,
                user_text=form.usertextoutput.data,
                safe_level=form.sflcalculation.data)]
            return render_template('results.html', title='Results', posts=results)
        except NoDataFoundForCode as info:
            return render_template('error.html', title="No Data Found", error=info)
